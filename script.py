#!/usr/bin/env python
#coding: utf-8
import os
import sys
import fcntl
import pty
import tty
import termios
import select

def	child():
	t = ["/bin/sh", "-i"]
	print os.execv(t[0], t)
	print "child: execv failed"

def	master_loop(fd):
	print "master loop: fd =", fd
	#os.write(fd, "ls /dev\n")
	while True:
		r, w, x = select.select([fd, 0], [], [], 0)
		if 0 in r:
			s = os.read(0, 1024)
			if not s:
				break
			os.write(fd, s)
			#os.write(1, "+"+s)
		if fd in r:
			s = os.read(fd, 1024)
			if not s:
				break
			os.write(1, s)

def	main():
	p = termios.tcgetattr(0)
	tty.setraw(0)
	pid, master = pty.fork()
	print pid, master
	if pid == 0:
		child()
	try:
		master_loop(master)
	except:
		print "master_loop terminated\r"
	termios.tcsetattr(0, termios.TCSANOW, p)

if __name__ == "__main__":
	main()
